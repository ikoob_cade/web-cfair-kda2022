import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'linkify'
})
export class LinkifyPipe implements PipeTransform {

    constructor(private _domSanitizer: DomSanitizer) { }

    transform(value: any, args?: any): any {
        return this._domSanitizer.bypassSecurityTrustHtml(this.stylize(value));
    }

    // 하이퍼링크 파이프
    private stylize(text: string): string {
        let stylizedText: string = '';
        if (text && text.length > 0) {
            for (let t of text.split(" ")) {
                if (t.startsWith("http") && t.length > 1) {
                    stylizedText += `<a target="_blank" href="${t.substring(0)}">${t}</a> `;
                } else {
                    stylizedText += t + " ";
                }
            }
            return stylizedText;
        } else {
            return text;
        }
    }
}

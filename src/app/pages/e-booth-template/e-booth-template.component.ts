import * as _ from 'lodash';
import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BoothService } from '../../services/api/booth.service';
import { MemberService } from '../../services/api/member.service';
import { environment } from '../../../environments/environment';

declare var $: any;
@Component({
  selector: 'app-e-booth-template',
  templateUrl: './e-booth-template.component.html',
  styleUrls: ['./e-booth-template.component.scss']
})
export class EBoothTemplateComponent implements OnInit, AfterViewInit {
  public selectedBooth = null;
  public attachments = [];
  public random = 0;
  public isLoaded = false;

  constructor(
    private boothService: BoothService,
  ) {
    // this.getVisitBooth();
  }

  ngOnInit(): void {
    $('#e-boothModal').on('hidden.bs.modal', () => {
      this.selectedBooth = null;
      document.getElementById('boothModalDesc').innerHTML = null;
    });
  }

  ngAfterViewInit(): void {
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  // 모달의 부스내용을 선택한 아이템 데이터로 변경
  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  // 부스 선택
  getDetail = (selectedBoothId) => {
    const customCondition = {
      isStampLimit: 150,
      isStampReset: false,
      isStampStrDate: '2022-05-13',
      isStampEndDate: '2022-05-14',
      isStampGrade: true,
      passGradeParam: 'Regular',
      passCntParam: 16,
    };

    this.boothService
      .findOne(selectedBoothId,null, false, customCondition)
      .subscribe(res => {
        this.selectedBooth = res;
        this.random = Math.floor(Math.random() * 10);
        this.setAttachments();
        this.setDesc();

        if (selectedBoothId === '626a09fd9ad5c3001221f7aa') {
          window.open('https://www.diabetes.or.kr', '_blank');
        } else {
          $('#e-boothModal').modal('show');
        }
      });
  }
}

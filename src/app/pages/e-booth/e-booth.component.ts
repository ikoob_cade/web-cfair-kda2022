import * as _ from 'lodash';
import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BoothService } from '../../services/api/booth.service';
import { MemberService } from '../../services/api/member.service';
import { CookieService } from 'ngx-cookie-service';
import { FunctionService } from '../../services/function/function.service';
import { environment } from '../../../environments/environment';

declare var $: any;
@Component({
  selector: 'app-e-booth',
  templateUrl: './e-booth.component.html',
  styleUrls: ['./e-booth.component.scss']
})
export class EBoothComponent implements OnInit, AfterViewInit {
  @ViewChild('winAlertBtn') winAlertBtn: ElementRef;
  @ViewChild('initAlertBtn') initAlertBtn: ElementRef;
  @ViewChild('notiAlertBtn') notiAlertBtn: ElementRef;

  public sponsors: Array<any> = []; // 스폰서 목록
  private categories: Array<any> = [];
  public categories1: Array<any> = []; // 카테고리[부스] 목록
  public categories2: Array<any> = []; // 카테고리[부스] 목록
  public user: any = JSON.parse(sessionStorage.getItem('cfair'));
  public selectedBooth = null;
  public boothsOfStamp: Array<any>[];
  public attachments = [];
  public random = 0;

  public isLoaded = false;

  private stampTourAccess = true;

  constructor(
    private boothService: BoothService,
    private memberService: MemberService,
    private cookieService: CookieService,
    private functionService: FunctionService
  ) {
    // this.getVisitBooth();
  }

  ngOnInit(): void {
    // this.attachments = this.setAttachments();
    $('#e-boothModal').on('hidden.bs.modal', () => {
      this.selectedBooth = null;
      document.getElementById('boothModalDesc').innerHTML = null;
    });
  }

  ngAfterViewInit(): void {
    // const cookieTime = this.cookieService.get(`${environment.eventId}_booth_init`);
    // const now = new Date().getTime().toString();
    // if (cookieTime < now) {
    //   this.cookieService.delete(`${environment.eventId}_booth_init`);
    //   this.initAlertBtn.nativeElement.click();
    // }
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  notOpenOneDay(): void {
    const afterHalfDay = new Date();
    afterHalfDay.setHours(afterHalfDay.getHours() + 12);
    this.cookieService.set(`${environment.eventId}_booth_init`, afterHalfDay.getTime().toString(), { expires: afterHalfDay });
  }


  /**
   * 부스 목록 조회
   *
   * atweek2020 작업중 작성
   * 카드형식의 업체도 스탬프투어가 적용되어야할경우 부스관리에 등록 후 배열을 직접 잘라서(slice) html에 배치.
   */
  loadBooths = () => {
    this.boothService.find()
      .subscribe(res => {
        const booths = res.slice(0, 20);
        if (res.length < 1) {
          return;
        }
        this.categories =
          _.chain(booths)
            .groupBy(booth => {
              return booth.category ? JSON.stringify(booth.category) : '{}';
            })
            .map((booth, category) => {
              category = JSON.parse(category);
              category.booths = booth;
              return category;
            }).sortBy(category => {
              return category.seq;
            })
            .value();

        this.categories = _.sortBy(this.categories, 'seq');
        _.forEach(this.categories, category => {
          category.categoryName = category.categoryName.toLowerCase();
        });

        this.categories1 = this.categories;
        this.categories2 = res.slice(15, 29);
        this.isLoaded = true;
      });
  }

  // 모달의 부스내용을 선택한 아이템 데이터로 변경
  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  goTop(): void {
    this.functionService.scrollToTop();
  }

  // 부스 선택
  getDetail = (selectedBoothId) => {
    /**
     *  ICDM2021용 커스텀 옵션
     * @param isStampLimit 선착순 300명 number
     * @param isStampReset true 매일초기화 boolean
     * @param isStampGrade 해당 categoryName 등급을 제거해줌.
     * @param isStampStartDate 2021-10-08 YYYY-MM-DD
     * @param isStampEndDate 2021-10-09 YYYY-MM-DD
     *
     */
    const customCondition = {
      isStampLimit: 300,
      isStampReset: true,
      isStampStrDate: '2022-05-13',
      isStampEndDate: '2022-05-14',
      isStampGrade: true,
      passGradeParam: 'Regular',
      passCntParam: 16,
    };

    this.boothService
      .findOne(selectedBoothId, this.user ? this.user.id : null, this.stampTourAccess, customCondition)
      .subscribe(res => {
        this.selectedBooth = res;
        this.random = Math.floor(Math.random() * 10);
        this.setAttachments();
        this.setDesc();

        if (selectedBoothId === '626a09fd9ad5c3001221f7aa') {
          window.open('https://www.diabetes.or.kr', '_blank');
        } else {
          $('#e-boothModal').modal('show');
        }

        if (res.tourSuccess) {
          this.winAlertBtn.nativeElement.click();
        }

      });
  }
}

import * as _ from 'lodash';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { VodService } from '../../../services/api/vod.service';
import { SpeakerService } from '../../../services/api/speaker.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../../environments/environment';
import { DateService } from '../../../services/api/date.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-vod',
  templateUrl: './vod.component.html',
  styleUrls: ['./vod.component.scss']
})
export class VodComponent implements OnInit, AfterViewInit {

  categories = [];
  selectSpeaker: any;
  dates = [];
  searched: Boolean;
  selectedDateId;
  searchKeyword= "";

  page = 1;
  pageSize = 20;
  totalPagesCount = 0;
  totalCount = 0;

  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;

  constructor(
    private vodService: VodService,
    private speakerService: SpeakerService,
    public router: Router,
    private cookieService: CookieService,
    private dateService: DateService,
    private fb: FormBuilder,
  ) {
    this.search = this.fb.group({
      keywordSelect: ['title'],
      keyword: [''],
    });
  }

  search;
  ngOnInit(): void {
    this.getDates();
  }

  submit() {
    this.searched = true;
    this.loadVods();
  }

  changePage(page){
    this.page = page;
    this.loadVods();
  }

  changeDate() {
    this.search.controls.keyword.setValue();
    this.searchKeyword = '';
    this.loadVods();
  }

  ngAfterViewInit(): void { }

  /** 날짜 목록 조회 */
  getDates(): void {
    this.dateService.find().subscribe(res => {
      this.dates = res;
      this.selectedDateId = this.dates[0].id;

      this.loadVods();
    });
  }

  /**
   * VOD 목록 조회
   */
  public loadVods = () => {
    const params = {
      page: this.page,
      pageSize: this.pageSize,
      keyword: this.search.controls.keyword.value,
    }
    //검색, 페이징 포함 목록조회
    this.vodService.findVodList(params, this.selectedDateId).subscribe(res => {
      const categories = this.sortByCategory(res.vods);
      this.totalPagesCount = res.totalPagesCount;
      this.totalCount = res.totalCount
      this.searchKeyword = this.search.controls.keyword.value;
      
      categories.forEach((category) => {
        const groups = {};
        category.vods.forEach((vod) => {
          if (vod.group) {
            if (!groups[vod.group]) {
              groups[vod.group] = [vod];
            } else {
              groups[vod.group].push(vod);
            }
          }
        });
        const groupList = [];

        // tslint:disable-next-line: forin
        for (const i in groups) {
          groupList.push(groups[i]);
        }

        if (groupList.length > 0) {
          category.groups = groupList;
        }
      });
      this.categories = categories;
    });
  }

  /**
   * 각 VOD에 카테고리가 있을 경우 카테고리별로 정렬해서 return
   */
  sortByCategory = (vods) => {
    return _.chain(vods)
      .groupBy(vod => {
        return vod.category ? JSON.stringify(vod.category) : '{}';
      })
      .map((vod, category) => {
        category = JSON.parse(category);
        category.vods = vod;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }


  goDetail(vod): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

  /**
   * 발표자 LIVE / VOD 리스트 조회
   */
  getDetail = (selectSpeaker) => {
    this.speakerService.findOne(selectSpeaker.id)
      .subscribe(res => {
        this.selectSpeaker = res;
      });
  }

  noModal(): void {
    this.cookieService.set(`${environment.eventId}_vod_popup`, 'true', { expires: new Date(new Date().getTime() + 1000 * 60 * 60) });
  }
}

import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { VodService } from '../../services/api/vod.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit, OnDestroy {
  @Input('isPage') isPage = true;
  
  public vods: any[] = [];
  public categories: any;

  public user;
  constructor(
    private router: Router,
    private vodService: VodService
  ) {
    this.getVod();
  }

  private koreaTimer;
  public koreaTime;

  ngOnInit(): void {
    this.user = sessionStorage.getItem('cfair');
  }

  ngOnDestroy(): void {
    clearInterval(this.koreaTimer);
  }

  public getVod(): void {
    this.vodService.find().subscribe((response: any) => {
      this.categories = this.sortByCategory(response);
      // this.vods = response;
    });
  }

  public goDetail(vod: any): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

  sortByCategory = (posters) => {
    return _.chain(posters)
      .groupBy(poster => {
        return poster.category ? JSON.stringify(poster.category) : '{}';
      })
      .map((poster, category) => {
        category = JSON.parse(category);
        category.vods = poster;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }

}

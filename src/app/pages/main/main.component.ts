import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewEncapsulation,
} from '@angular/core';
import { EventService } from '../../services/api/event.service';
import { BannerService } from '../../services/api/banner.service';
import { DayjsService } from '../../services/dayjs.service';
import { FunctionService } from '../../services/function/function.service';
declare var $: any;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MainComponent implements OnInit, AfterViewInit {
  public showAbout = true; // 인사말 컴포넌트 사용 여부
  public showProgram = true; // 프로그램 (타임테이블) 컴포넌트 사용 여부
  public showSpeaker = true; // 발표자 컴포넌트 사용 여부

  public event; // 행사정보 데이터
  public mainBanners: any = []; // 메인배너 목록

  constructor(
    private bannerService: BannerService,
    private dayJsService: DayjsService,
    private functionService: FunctionService,
    private eventService: EventService,
  ) { }

  ngOnInit(): void {
    this.eventService.findOne().subscribe((res) => {
      this.event = res;
      this.functionService.checkEventVersion(this.event);
    });
  }

  ngAfterViewInit(): void {
    // * 메인팝업 */
    // this.alertService.mainPopupWithCookie(this.entryAlert);
    this.setSystemMaintenance();
    // this.getMainBanners();
  }

  // ! Banner Slider Controller
  /** 배너 목록 조회 */
  getMainBanners(): void {
    this.bannerService.find('main-slider').subscribe(res => {
      const bannersData = [];
      if (res.length > 0) {
        res.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          bannersData.push(data);
        });

        this.mainBanners = bannersData;
      }
    });
  }

  /** 점검안내용 모달출력 함수 */
  setSystemMaintenance(): void {
    const first = this.dayJsService.makeTime('2022-05-11 15:00');
    const end = this.dayJsService.makeTime('2022-05-11 18:00'); // 출력 기간 하드 설정
    const now = this.dayJsService.makeTime();

    if (now.isSameOrAfter(first) && now.isSameOrBefore(end)) {
      setTimeout(() => {
        $('#modal-maintenance-alert').modal('show');
      }, 0);
    }
  }
}

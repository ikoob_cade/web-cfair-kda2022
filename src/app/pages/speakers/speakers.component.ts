import * as _ from 'lodash';
import { Component, OnInit, HostListener } from '@angular/core';
import { CategoryService } from '../../services/api/category.service';
import { SpeakerService } from '../../services/api/speaker.service';
import { Router } from '@angular/router';

declare let $: any;
@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {

  public speakers: Array<any> = [];
  public mobile: boolean = false;

  public speakerCategories: Array<any> = [];

  public speakerDetail: any = {};
  public speakerLoaded = false;

  public user = sessionStorage.getItem('cfair');

  public speakerRoles: any = {
    chairman: false,
    speaker: false,
    international: false,
  }; // 발표자 역할목록
  public isOnlyPanel: boolean = false;

  public speakerCategory: String = '';

  constructor(
    private speakerService: SpeakerService,
    private categoryService: CategoryService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.mobile = window.innerWidth < 768;
    // this.loadCategories();
    this.loadSpeakers();
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.mobile = window.innerWidth < 768;
  }


  // 발표자 리스트를 조회한다.
  loadSpeakers = () => {
    this.speakerService.find(false).subscribe(res => {
      this.speakers = res;
      this.loadCategories();
    });
  }

  loadCategories = () => {
    this.categoryService.findByCategoryIds('speaker').subscribe(res => {
      _.forEach(res, category => {
        category.speakers = _.sortBy(category.speakers, 'seq');
      });
      this.speakerCategories = res;
    });
  }

  routeLecture(lecture): void {
    this.router.navigate(['/live'], { queryParams: { dateId: lecture.dateId, roomId: lecture.roomId } });
  }


  getDetail(speaker, category): void {
    setTimeout(() => {
      this.speakerLoaded = false;
    }, 0);

    if (this.user) {
      this.speakerService.findOne(speaker.id).subscribe(res => {
        this.speakerDetail = res;
        this.speakerCategory = category;

        this.speakerRoles = {
          chairman: false,
          speaker: false,
          international: false,
        };

        if (this.speakerDetail.roles) {
          const fSpeakerRoles = _.split(this.speakerDetail.roles, '|');
          _.forEach(fSpeakerRoles, role => {
            this.speakerRoles[role] = true;
          });
        }

        if (this.speakerRoles['panel'] && !this.speakerRoles['speaker'] && !this.speakerRoles['chairman'] || this.speakerDetail.fullName === '이종순' || this.speakerDetail.fullName === '김종화') {
          this.isOnlyPanel = true;
        } else { 
          this.isOnlyPanel = false;
        }

        if (this.speakerDetail.fullName === '조영민') {
          this.speakerDetail.isCustom = true;
          this.speakerDetail.filename1 = 'Curriculum Vitae (S1 Clinical diabetes and therapeutics 1)';
          this.speakerDetail.filename2 = 'Curriculum Vitae (Luncheon symposium 4)';
        } else if (this.speakerDetail.fullName === '문선준') {
          this.speakerDetail.isCustom = true;
          this.speakerDetail.filename1 = 'Curriculum Vitae (S5 Clinical diabetes and therapeutics 2)';
          this.speakerDetail.filename2 = 'Curriculum Vitae (S11 Clinical diabetes and therapeutics 4)';
        } else if (this.speakerDetail.fullName === '진상만') {
          this.speakerDetail.isCustom = true;
          this.speakerDetail.filename1 = 'Curriculum Vitae (S11 Clinical diabetes and therapeutics 4)';
          this.speakerDetail.filename2 = 'Curriculum Vitae (Luncheon symposium 1)';
        } else if (this.speakerDetail.fullName === '이현승') {
          this.speakerDetail.isCustom = true;
          this.speakerDetail.filename1 = 'Curriculum Vitae (S14 Translational research 2)';
          this.speakerDetail.filename2 = 'Curriculum Vitae (DMJ International publication award)';
        } else if (this.speakerDetail.fullName === '오태정') {
          this.speakerDetail.isCustom = true;
          this.speakerDetail.filename1 = 'Curriculum Vitae (DMJ International publication award)';
          this.speakerDetail.filename2 = 'Curriculum Vitae (Satellite symposium 4)';
        } else if (this.speakerDetail.fullName === '조호찬') {
          this.speakerDetail.isCustom = true;
          this.speakerDetail.filename1 = 'Curriculum Vitae (보험-대관위원회 세션)';
          this.speakerDetail.filename2 = 'Curriculum Vitae (Satellite symposium 2)';
        } else if (this.speakerDetail.fullName === '김상용') {
          this.speakerDetail.isCustom = true;
          this.speakerDetail.filename1 = 'Curriculum Vitae (진료지침위원회 세션)';
          this.speakerDetail.filename2 = 'Curriculum Vitae (Luncheon symposium 5)';
        }

        this.speakerLoaded = true;
        $('#speakersModal').modal('show');
      });
    } else {
      this.router.navigate(['/login']);
    }
  }
}

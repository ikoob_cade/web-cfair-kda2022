import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UAParser } from 'ua-parser-js';
import { DayjsService } from '../../services/dayjs.service';
import { FunctionService } from '../../services/function/function.service';
import { MemberService } from '../../services/api/member.service';
declare var $: any;

enum AUTH_ERROR_MESSAGE {
  UNAUTHORIZED = 'Email,  Password not matched.',
  NOT_FOUND = 'Email,  Password not matched.',
  ENTER_ALL = 'Please enter the email address & password',
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  @ViewChild('entryAlertBtn') entryAlert: ElementRef;

  private user: any;
  public passwordError = ''; // 로그인 에러메세지
  public member = {
    email: '',
    password: '',
  };

  public pwdEmail = ''; // 패스워드 찾기의 이메일 input
  public findPasswordError = '';

  constructor(
    public router: Router,
    public auth: AuthService,
    private dayJsService: DayjsService,
    private functionService: FunctionService,
    private memberService: MemberService
  ) { }

  async ngOnInit(): Promise<void> {
    await this.functionService.checkEventVersion();
  }

  ngAfterViewInit(): void {
    this.setSystemMaintenance();
  }

  setSystemMaintenance(): void {
    const first = this.dayJsService.makeTime('2022-01-22 11:00');
    const end = this.dayJsService.makeTime('2022-01-22 11:20');
    const now = this.dayJsService.makeTime();

    if (now.isSameOrAfter(first) && now.isSameOrBefore(end)) {
      // ! 메인 팝업
      setTimeout(() => {
        this.entryAlert.nativeElement.click();
      }, 0);
    }

  }

  async login(): Promise<void> {
    if (this.loginValidator()) {
      const notIn = !([
        'ikoob1@ikoob1.com',
        'ikoob2@ikoob2.com',
        'ikoob3@ikoob3.com',
        'planbear01@planbear.com',
        'planbear02@planbear.com',
        'planbear03@planbear.com',
        'planbear04@planbear.com',
        'planbear05@planbear.com',
        'areum.park@ikoob.com',
        'jinhyuc.ko@ikoob.com',
        'jinhyuc.ko2@ikoob.com',
        'jinhyuc.ko3@ikoob.com',
        'jinhyuc.ko4@ikoob.com',
        'jiyeon.kim@ikoob.com',
        'sungcheon.lee@ikoob.com',
        'ikoob1@ikoob.com',
        'drhopper@ikoob.com',
        'rope557@naver.com',
        'silverlhj@hanmail.net',
        'diabetes01@planbear.co.kr',
        'diabetes02@planbear.co.kr',
        'diabetes03@planbear.co.kr',
        'diabetes04@planbear.co.kr',
        'diabetes05@planbear.co.kr',
        'diabetes06@planbear.co.kr',
        'diabetes07@planbear.co.kr',
        'diabetes08@planbear.co.kr',
        'diabetes09@planbear.co.kr',
        'diabetes10@planbear.co.kr',
        'diabetes11@planbear.co.kr',
        'diabetes12@planbear.co.kr',
        'diabetes13@planbear.co.kr',
        'diabetes14@planbear.co.kr',
        'diabetes15@planbear.co.kr',
        'diabetes16@planbear.co.kr',
        'diabetes17@planbear.co.kr',
        'diabetes18@planbear.co.kr',
        'diabetes19@planbear.co.kr',
        'diabetes20@planbear.co.kr',
        'diabetes21@planbear.co.kr',
        'diabetes22@planbear.co.kr',
        'diabetes23@planbear.co.kr',
        'diabetes24@planbear.co.kr',
        'diabetes25@planbear.co.kr',
        'diabetes26@planbear.co.kr',
        'diabetes27@planbear.co.kr',
        'diabetes28@planbear.co.kr',
        'diabetes29@planbear.co.kr',
        'diabetes30@planbear.co.kr'
      ].includes(this.member.email));

      if (notIn) {
        const isBefore = await this.memberService.compareServerTime('2022-05-12 15:00:00');
        if (isBefore) {
          return alert('5.12 (목) 오후 3시부터 로그인하실 수 있습니다.');
        }
      }

      const parser = new UAParser();
      const fullUserAgent = parser.getResult();

      const body: any = {
        loginId: this.member.email,
        password: this.member.password,
        userAgent: JSON.stringify(fullUserAgent),
        browser: JSON.stringify(fullUserAgent.browser),
        device: JSON.stringify(fullUserAgent.device),
        engine: JSON.stringify(fullUserAgent.engine),
        os: JSON.stringify(fullUserAgent.os),
        ua: JSON.stringify(fullUserAgent.ua),
      };

      this.auth.login(body).subscribe(
        async (res) => {
          if (res.token) {
            this.user = res;
            this.passwordError = '';
            // this.policyAlertBtn.nativeElement.click();

            this.goToMain();
          }
        },
        (error) => {
          if (error.status === 401) {
            this.passwordError = AUTH_ERROR_MESSAGE.UNAUTHORIZED;
          }
          if (error.status === 404) {
            this.passwordError = AUTH_ERROR_MESSAGE.NOT_FOUND;
          }
        }
      );
    } else {
      this.passwordError = AUTH_ERROR_MESSAGE.ENTER_ALL;
    }
  }

  loginValidator(): boolean {
    if (!this.member.email || !this.member.password) {
      return false;
    }
    return true;
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    sessionStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }

  openModal(): void {
    this.policyAlertBtn.nativeElement.click();
  }

  /**
   * 초기화 이메일을 전송한다.
   */
  sendEmail(): void {
    if (this.pwdEmail) {
      // TODO if(이메일정규식 검사) findPasswordError = "이메일 형식이 아닙니다."
      this.auth.sendResetEmail(this.pwdEmail).subscribe(res => {
        if (res) {
          alert(`We sent you an email with Temporary password.\n
            If you haven’t received this email within a few minutes,\n
            Please your spam folder`);
          this.cancelFindPwd();
        }
      }, error => {
        if (error.status === 404) {
          this.findPasswordError = 'Email address not matched, please contact KDA 2022 event Agency.';
        }
      });
    } else {
      this.findPasswordError = 'Please enter the email you register to KDA 2022';
    }
  }

  /**
  * 비밀번호 찾기 닫기
  */
  cancelFindPwd = () => {
    this.findPasswordError = '';
    $('#findPassword').modal('hide');
    this.pwdEmail = '';
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SurveyComponent implements OnInit {
  constructor() { }

  ngOnInit(): void { }

}

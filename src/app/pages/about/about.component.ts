import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { EventService } from '../../services/api/event.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AboutComponent implements OnInit {
  @Input('isPage') isPage: boolean = true;
  @Input('description') description: any;

  public event;

  constructor(
    private eventService: EventService,
  ) {
  }

  ngOnInit(): void {
    if (!this.description) {
      this.loadEventInfo();
    }
  }

  loadEventInfo = () => {
    this.eventService.findOne().subscribe(res => {
      this.description = res.description2;
    });
  }

}

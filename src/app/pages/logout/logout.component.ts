import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocketService } from '../../services/socket/socket.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {
  constructor(
    private router: Router,
    private authService: AuthService,
    private socketService: SocketService
  ) { }
  private user;

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));

    if (!this.user) {
      this.router.navigate(['/main']);
    }

    this.logout();
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  /** 소켓 로그아웃 */
  logoutSocket(): void {
    this.socketService.logout({
      memberId: this.user.id
    });
  }
}

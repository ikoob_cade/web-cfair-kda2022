import { Component, OnInit, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line: no-input-rename
  @Input('session') session: any; // 세션 정보
  @Output('detailFn') detailFn = new EventEmitter(); // 자세히보기
  @Output('sendLive') sendLive = new EventEmitter(); // 현재 세션

  public polling: any;
  public isLive: boolean = false;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.checkLive();
  }

  ngOnDestroy(): void {
    clearTimeout(this.polling);
  }

  // Check Live
  checkLive(): void {
    const agendaDate = this.session.date.date.replace(/-/gi, '/');
    const agendaStartTime = new Date(`${agendaDate}/${this.session.title.split('-')[0]}`);
    const agendaEndTime = new Date(`${agendaDate}/${this.session.title.split('-')[1]}`);
    const now: Date = new Date();
    if (now > agendaStartTime && now < agendaEndTime) {
      this.isLive = true;
      this.sendLive.emit(this.session);
    } else {
      this.isLive = false;
    }
    const intervalNum = 60; // TODO 1분 간격 갱신
    this.polling = setTimeout(() => { this.checkLive(); }, 1000 * intervalNum);
  }

}

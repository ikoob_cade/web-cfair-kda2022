import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, AfterContentChecked, OnDestroy } from '@angular/core';
import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { DateService } from '../../services/api/date.service';
import { RoomService } from '../../services/api/room.service';
import { Router } from '@angular/router';
import { DayjsService } from '../../services/dayjs.service';
import { AgendaService } from '../../services/api/agenda.service';
declare let $: any;
@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit, AfterContentChecked, OnDestroy {
  @ViewChild('sessionDetailBtn') sessionDetailBtn: ElementRef;
  @ViewChild('table1', { read: ElementRef }) table1: ElementRef;

  @ViewChild('th1', { read: ElementRef }) th1: ElementRef;

  public selectedDate = 2;
  private koreaTimer;
  public koreaTime = this.dayjsService.getNow().format('YYYY-MM-DD HH:mm:ss');
  private user = sessionStorage.getItem('cfair') ? JSON.parse(sessionStorage.getItem('cfair')) : null;
  public dates: any = [];
  private rooms: any = [];

  openMetaverse() {
    if (this.user) {
      window.open('https://zep.us/play/8rqJOD', '_blank');
    } else {
      this.router.navigate(['/login']);
    }
  }

  public selectedAgenda;
  public selectedSubList;

  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private dateService: DateService,
    private roomService: RoomService,
    private dayjsService: DayjsService,
    private agendaService: AgendaService,
  ) { }

  ngOnInit(): void {
    this.doInit();
    this.getTime();

    $(document).on('click', '.rid', function () {
      alert("this");
    });
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
  }

  setDate = (index: number) => {
    this.selectedDate = index;
  }

  doInit(): void {
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .subscribe(resp => {
        this.dates = resp[0];
        this.rooms = resp[1];
      });
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * Live 상세보기
   * @param date 순서
   * @param room 순서
   */
  goLive(date: number, room: number): void {
    console.log(this.selectedAgenda);
    this.router.navigate(['/live'], { queryParams: { dateId: this.dates[date].id, roomId: this.rooms[room].id } });
  }

  goLiveV2(dateId, roomId): void {
    this.router.navigate(['/live'], { queryParams: { dateId, roomId } });
  }

  goPoster(): void {
    this.router.navigate(['/posters']);
  }

  goVod(): void {
    this.router.navigate(['/vod']);
  }

  getTime(): void {
    this.koreaTimer = setInterval(() => {
      this.koreaTime = this.dayjsService.getNow().format('YYYY-MM-DD HH:mm:ss');
    }, 1000);
  }


  /** 세션 내 강의목록 조회 + MODAL */
  getSessionDetail(sessionId?: string): void {
    if (!sessionId) {
      return;
    } // 개발용
    if (this.user) {
      // 초기화
      this.selectedAgenda = null;
      this.selectedSubList = null;

      this.agendaService.findSessionSublist(sessionId).subscribe(res => {
        setTimeout(() => {
          this.selectedAgenda = res.agenda; // 클릭한 부모세션
          this.selectedSubList = res.subList; // 하위 강의 목록
          $('#modal-session-detail').modal('show');
        }, 500);
      });
    } else {
      this.router.navigate(['/login']); // 비로그인 시
    }
  }

  openUrl(url) {
    if (sessionStorage.getItem('cfair')) {
      window.open(url, '_blank');
    } else {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy(): void {
    clearInterval(this.koreaTimer);
  }

}

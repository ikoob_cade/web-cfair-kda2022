import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ChangeDetectorRef,
  ViewEncapsulation,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import * as _ from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';
import { NgImageSliderComponent } from 'ng-image-slider';
@Component({
  selector: 'app-player-youtube-ibm',
  templateUrl: './player-youtube-ibm.component.html',
  styleUrls: ['./player-youtube-ibm.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PlayerYoutubeIbmComponent implements OnInit, AfterViewInit {
  @Output('checkIsLastSession') checkIsLastSession = new EventEmitter(); // 마지막세션 확인
  @ViewChild('videoPlayer') videoPlayer: ElementRef;
  @Input('content') content: any; // 전달받은 콘텐츠 정보
  @Input('isLive') isLive = false; // 댓글 유무 확인 (상세에서는 채팅이 없기때문에 false로 받는다.);
  @Input('isVod') isVod = false;
  @Input('agendas') agendas: any;
  @Input('selected') selected: any; // 전달받은 날짜/룸(채널) 정보
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;

  public user: any;
  public reg = /vimeo.com/;
  public player: any;

  public liveUrl;
  public paginationConfig;
  public totalReplys = 0;
  public replyForm: FormGroup;
  public replys: any = [];
  public relationId: string;

  public chatProvider: any = '';

  /** 광고구좌 */
  public banners = [];

  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
  ) {
    this.replyForm = fb.group({
      content: [
        '',
        Validators.compose([Validators.required, Validators.minLength(10)]),
      ],
    });
  }

  ngOnInit(): void {
    if (this.isLive) {
      this.chatProvider = this.selected.room.contents.chatIncluded;
    } else {
      this.chatProvider = null;
    }

    this.liveUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.content.contentUrl);
    this.relationId = this.isLive ? this.selected.room.id : this.content.id;

    this.user = JSON.parse(sessionStorage.getItem('cfair'));
  }

  /** 채팅컴포넌트 측 발신 emit 수신 함수
   * chat-ikoob 컴포넌트 => 현재 컴포넌트 => live 컴포넌트
   */
  callCheckIsLastSession(serverTime): void {
    this.checkIsLastSession.emit(serverTime);
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

}

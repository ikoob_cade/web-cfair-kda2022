import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';
import { Router } from '@angular/router';

declare let $: any;
@Component({
  selector: 'app-speaker-slider',
  templateUrl: './speaker-slider.component.html',
  styleUrls: ['./speaker-slider.component.scss']
})
export class SpeakerSliderComponent implements OnInit {
  public user = sessionStorage.getItem('cfair');
  public speakerLoaded = false;
  public speakerDetail: any = {};
  public speakerCategory: String = '';

  public speakerRoles: any = {
    chairman: false,
    speaker: false,
    international: false,
  }; // 발표자 역할목록

  public speakers = []; // 발표자 목록
  public mainSpeakers = []; // 메인 발표자 목록

  constructor(
    public speakerService: SpeakerService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.mainSpeakers = [
      {
        profileUrl: '../../../assets/img/main_speaker1.png',
        category: 'Plenary lecture',
        id: '6261de2b1a3f9400121d946c',
        fullName: 'Juan Pablo Frias',
        company: 'National Research Institute, CA, USA',
        roles: 'Speaker'
      },
      { 
        profileUrl: '../../../assets/img/main_speaker2.png',
        category: 'Plenary lecture',
        id: '62660dfd1a3f9400121d94e6',
        fullName: '박병현',
        company: '전북의대 생화학분자 생물학교실',
        roles: 'Speaker'
      },
      { 
        profileUrl: '../../../assets/img/main_speaker3.png',
        category: 'Plenary lecture',
        id: '62660e291a3f9400121d94e7',
        fullName: '이인규',
        company: '경북의대 내과',
        roles: 'Speaker'
      }
    ]
  }

  /** 발표자 목록 조회 */
  getSpeakers(): void {
    this.speakerService.find().subscribe(res => {
      this.speakers = res;
    })
  }

  getDetail(speaker): void {
    this.speakerCategory = speaker.category;
    setTimeout(() => {
      this.speakerLoaded = false;
    }, 0);

    if (this.user) {
      this.speakerService.findOne(speaker.id).subscribe(res => {
        this.speakerDetail = res;

        this.speakerRoles = {
          chairman: false,
          speaker: false,
          international: false,
        };

        if (this.speakerDetail.roles) {
          const fSpeakerRoles = _.split(this.speakerDetail.roles, '|');
          _.forEach(fSpeakerRoles, role => {
            this.speakerRoles[role] = true;
          });
        }

        this.speakerLoaded = true;
        $('#speakersModal').modal('show');
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

  routeLecture(lecture): void {
    this.router.navigate(['/live'], { queryParams: { dateId: lecture.dateId, roomId: lecture.roomId } });
  }

}

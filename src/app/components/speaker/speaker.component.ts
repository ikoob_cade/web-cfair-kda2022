import * as _ from 'lodash';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';

interface Speaker {
  categoryIds: [];
  company: string;
  depart: string;
  fullName: string;
  roles: string;
  title: string;
  photoUrl: string; // URL
  CV: string; // URL
  abstract: string; // URL
}
@Component({
  selector: 'app-speaker',
  templateUrl: './speaker.component.html',
  styleUrls: ['./speaker.component.scss']
})
export class SpeakerComponent implements OnInit {
  @Input('speaker') speaker: Speaker;
  @Output('speakerDetailFn') detailFn = new EventEmitter(); // 자세히보기

  public speakerRoles: any = {
    chairman: false,
    speaker: false,
    international: false,
  }; // 발표자 역할목록

  constructor(
    public speakerService: SpeakerService,
  ) { }

  ngOnInit(): void {
    this.setSpeakerRoles();
  }

  setSpeakerRoles(): void {
    if (this.speaker.roles) {
      const fSpeakerRoles = _.split(this.speaker.roles, '|');
      _.forEach(fSpeakerRoles, role => {
        this.speakerRoles[role] = true;
      });
    }
  }

}

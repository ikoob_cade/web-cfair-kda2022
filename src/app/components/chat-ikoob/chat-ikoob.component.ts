import * as _ from 'lodash';
import {
  Component, OnInit, Input, ViewChild, ViewEncapsulation, Output, EventEmitter, OnDestroy
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MemberService } from '../../services/api/member.service';
declare let $: any;

@Component({
  selector: 'app-chat-ikoob',
  templateUrl: './chat-ikoob.component.html',
  styleUrls: ['./chat-ikoob.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChatIkoobComponent implements OnInit ,OnDestroy {
  @Output('checkIsLastSession') checkIsLastSession = new EventEmitter(); // 마지막세션 확인
  @Input('relationId') relationId: any;
  
  @ViewChild('commentList') commentList: any; // 댓글 목록
  public user: any;
  
  public liveUrl;
  public replyForm: FormGroup;
  public replys: any = [];
  private commentInterval; // 10초마다 채팅목록 조회 반복
  
  constructor(
    public fb: FormBuilder,
    private memberService: MemberService,
  ) {
    this.replyForm = fb.group({
      content: [
        '',
        Validators.compose([Validators.required, Validators.minLength(10)]),
      ],
    });
  }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    this.getComment();
    this.commentInterval = setInterval(() => {
      this.getComment();
    }, 10 * 1000);
  }

  ngOnDestroy(): void {
    clearInterval(this.commentInterval);
  }

  /** 댓글 불러오기 */
  getComment(): void {
    this.memberService
      .findComment(this.user.id, this.relationId)
      .subscribe((res) => {
        let isBottom = false;
        if (
          this.commentList.nativeElement.scrollTop ===
          this.commentList.nativeElement.scrollHeight -
          this.commentList.nativeElement.offsetHeight
        ) {
          isBottom = true;
        }

        if (res.length === 1 && !res[0].id) {
          this.replys = [];
        } else {
          this.replys = res;
        }
        
        this.checkIsLastSession.emit(res[0].now);
        setTimeout(() => {
          if (isBottom) {
            this.downScroll();
          }
        }, 800);
      });
  }

  /** 댓글달기 */
  comment(): void {
    if (!this.replyForm.value.content) {
      return alert('내용을 입력해주세요.');
    }
    this.memberService
      .createComment({
        memberId: this.user.id,
        title: '',
        content: this.replyForm.value.content,
        relationId: this.relationId,
      })
      .subscribe((res) => {
        this.getComment();
        // this.replyForm.value.content = '';
        this.replyForm.patchValue({
          content: '',
        });
        this.downScroll();
      });
  }

  /** 채팅창을 최하단으로 스크롤한다. */
  downScroll(): void {
    this.commentList.nativeElement.scrollTop =
      this.commentList.nativeElement.scrollHeight;
  }

  /** 본인이 입력한 채팅 여부 (삭제 출력) */
  check(comment): boolean {
    if (comment && comment.memberId === this.user.id) {
      return true;
    }
    return false;
  }

  /** 채팅을 삭제한다. */
  delete(comment): void {
    $('.dropdown-toggle').dropdown('dispose');
    setTimeout(() => {
      if (confirm('삭제하시겠습니까?')) {
        this.memberService
          .deleteComment(this.user.id, comment.id)
          .subscribe((res) => {
            this.getComment();
          });
      }
    }, 150);
  }
}

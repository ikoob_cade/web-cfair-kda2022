import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from '../../services/api/member.service';
import { AuthService } from '../../services/auth/auth.service';
import { EventService } from '../../services/api/event.service';
import { SocketService } from '../../services/socket/socket.service';
import { FunctionService } from '../../services/function/function.service';
import { HistoryService } from '../../services/api/history.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public user: any;
  public menus: any = [
    { title: 'Welcome', router: '/about' },
    { title: 'Program', router: '/program' },
    { title: 'LIVE', router: '/live' },
    // { title: 'VOD', router: '/vod' },
    { title: 'Speakers', router: '/speakers' },
    { title: 'Posters (Metaverse)', router: '/posters' },
    { title: 'Sponsors', router: '/e-booth' },
    { title: 'Notice', router: '/board' },
  ];

  public isLive = false;
  public activeAbstractBtn: any = false;

  collapse = true;

  event;
  constructor(
    private router: Router,
    private eventService: EventService,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
    private functionService: FunctionService,
    private historyService: HistoryService,
  ) {
    router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(sessionStorage.getItem('cfair'));
        this.user = auth && auth.token ? auth : undefined;
        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;

        this.functionService.scrollToTop();

        if (router.url === '/live') {
          this.isLive = true;
        } else {
          this.isLive = false;
        }
      });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip({
        template: `<div class="tooltip poster-tooltip" role="tooltip">
          <div class="arrow"></div>
          <div class="tooltip-inner">
          <span><b></b></span>
          </div>
        </div>`
      });
    });
  }

  ngOnInit(): void {
    this.eventService.findOne().subscribe((res) => (this.event = res));
  }

  ngOnDestroy(): void { }

  postAttendance = (): void => {
    this.memberService.attendance(this.user.id).subscribe((res) => { });
  }

  // 유저데이터 제거
  private removeUser(): void {
    this.logoutSocket();
    sessionStorage.removeItem('cfair');
    this.user = null;
  }

  /** 로그아웃 */
  logout(): void {
    if (window.confirm('로그아웃 하시겠습니까?')) {
      const currentRoom = this.historyService.getRoom();
      const attendance = this.historyService.getAttendance();
      // 퇴장을 남겨야 할 경우만 동작
      if (currentRoom && attendance === 'out') {
        const options: {
          relationId: string;
          relationType: string;
          logType: string;
        } = {
          relationId: currentRoom.id,
          relationType: 'room',
          logType: this.historyService.getAttendance(),
        };

        this.memberService.history(this.user.id, options).subscribe(
          () => {
            this.historyService.setAttendance('in');
            this.router
              .navigate(['/login'])
              .catch(() => {
                this.removeUser();
              })
              .finally(() => {
                this.removeUser();
              });
          },
          (error) => {
            console.error(error);
            this.router
              .navigate(['/login'])
              .catch(() => {
                this.removeUser();
              })
              .finally(() => {
                this.removeUser();
              });
          }
        );
      } else {
        this.router
          .navigate(['/login'])
          .catch((error) => {
            this.removeUser();
          })
          .finally(() => {
            this.removeUser();
          });
      }
    }
  }

  loginSocket() {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token,
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id,
    });
  }

  showTooltip(menuTitle): String {
    if (menuTitle === 'Posters') {
      return `<b>메타버스 전시안내</b><br>` + `포스터 전시는 메타버스 내에 있는 포스터 전시장으로 이동합니다.`;
    }
  }

  openMetaverse() {
    if (this.user) {
      window.open('https://zep.us/play/8rqJOD', '_blank');
    } else {
      this.router.navigate(['/login']);
    }
  }
}

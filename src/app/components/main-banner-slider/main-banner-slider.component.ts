import * as _ from 'lodash';
import { Component, HostListener, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SliderControllerService } from '../../services/function/sliderController.service';
import { NgImageSliderComponent } from 'ng-image-slider';

@Component({
  selector: 'app-main-banner-slider',
  templateUrl: './main-banner-slider.component.html',
  styleUrls: ['./main-banner-slider.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MainBannerSliderComponent implements OnInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  @ViewChild('bannerSliderMobile') bannerSliderMobile: NgImageSliderComponent;

  @Input('banners') banners: any;

  constructor(
    private sliderControllerService: SliderControllerService,
  ) {
  }

  public isMobile = window.innerWidth < 768;
  public mobileBanners = [
    {
      link: '',
      thumbImage: 'assets/img/Mobile-Banner-1.png',
      alt: '',
    },
    {
      link: 'http://www.eoflow.com/main/main.html',
      thumbImage: 'assets/img/Mobile-Banner-2.png',
      alt: '',
    },
    {
      link: 'https://eopatch.com/main/main.html',
      thumbImage: 'assets/img/Mobile-Banner-3.png',
      alt: '',
    }
  ];

  ngOnInit(): void {
  }

  /** 광고 구좌 왼쪽 버튼 클릭 */
  slidePrev(target): void {
    this.sliderControllerService.slidePrev(this, target);
  }
  /** 광고 구좌 오른쪽 버튼 클릭 */
  slideNext(target): void {
    this.sliderControllerService.slideNext(this, target);
  }
  /** 광고 배너 클릭 */
  imageClick(index): void {
    this.sliderControllerService.imageClick(this.banners, index);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = window.innerWidth < 768;
  }
}

import { Component, Input, OnInit, ViewChild, ViewEncapsulation, } from '@angular/core';
import { SliderControllerService } from '../../services/function/sliderController.service';
import { BannerService } from '../../services/api/banner.service';
import { NgImageSliderComponent } from 'ng-image-slider';

@Component({
  selector: 'app-banner-slider',
  templateUrl: './banner-slider.component.html',
  styleUrls: ['./banner-slider.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BannerSliderComponent implements OnInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  @Input('canMove') canMove = true;
  @Input('maxWidth') maxWidth: string;
  @Input('autoSlide') autoSlide?: number;
  @Input('bannerType') bannerType: string; // live, login-bottom, vod

  public banners: any[] = [];

  constructor(
    private sliderControllerService: SliderControllerService,
    private bannerService: BannerService,
  ) {
    if (!this.autoSlide) { this.autoSlide = 4; }
  }
  ngOnInit(): void {
    this.getBanners(this.bannerType);
  }

  // ! Banner Slider Controller
  /** 배너 목록 조회 */
  getBanners(bannerType): void {
    this.bannerService.find(bannerType).subscribe(res => {
      const bannersData = [];
      if (res.length > 0) {
        res.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          bannersData.push(data);
        });
        this.banners = bannersData;
      }
    });
  }

  /** 광고 구좌 왼쪽 버튼 클릭 */
  slidePrev(target): void {
    this.sliderControllerService.slidePrev(this, target);
  }
  /** 광고 구좌 오른쪽 버튼 클릭 */
  slideNext(target): void {
    this.sliderControllerService.slideNext(this, target);
  }
  /** 광고 배너 클릭 */
  imageClick(index): void {
    this.sliderControllerService.imageClick(this.banners, index);
  }

}

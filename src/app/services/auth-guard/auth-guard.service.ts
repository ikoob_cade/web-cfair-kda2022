import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MemberService } from '../api/member.service';
import { DayjsService } from '../dayjs.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private memberService: MemberService,
    private dayJsService: DayjsService
  ) { }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const auth = JSON.parse(sessionStorage.getItem('cfair'));
    if (auth && auth.token) {
      if (route.routeConfig.path === 'live') {
        if (auth.roles) {
          const isAdmin = auth.roles.includes('ROLE_CLIENT_VOD');
          if (!isAdmin) {
            const isBefore = await this.memberService.compareServerTime('2022-05-30 09:00:00');
            if (isBefore) {
              alert('5월 30일 9시부터 접근가능합니다.');
              return false;
            }
          }
        }
      }

      return true;
    } else {
      // 아래 path는 로그인 상태에서만 접근 가능
      // const path = ['live', 'vod', 'vod/:vodId', 'posters', 'posters/:posterId', 'my-page'];
      const path = ['live', 'vod', 'vod/:vodId', 'my-page', 'survey'];

      const result = path.indexOf(route.routeConfig.path);
      if (result > -1) {
        this.router.navigate(['/login']);
      }

      return true;
    }
  }
}


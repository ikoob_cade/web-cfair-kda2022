import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})

export class VodService {
  private serverUrl = '/events/:eventId/vod';
  private vodSearchUrl = '/events/:eventId/vod-date-pagination'
  constructor(private http: HttpClient) { }
  //61f232b01b8f6a1487f7a409?page=1&pageSize=20&keyword=test

  find(): Observable<any> {
    return this.http.get(this.serverUrl)
      .pipe(catchError(this.handleError));
  }

  findVodList(params,dateId): Observable<any> {
    let requestUrl = `${this.vodSearchUrl}/${dateId}`;
    return this.http.get(requestUrl,{params: params})
      .pipe(catchError(this.handleError));
  }

  findOne(vodId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + vodId)
      .pipe(catchError(this.handleError));
  }

  // ! 댓글
  findComments(vodId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + vodId + '/comment')
      .pipe(catchError(this.handleError));
  }

  createComment(vodId: string, body: any): Observable<any> {
    return this.http.post(this.serverUrl + '/' + vodId + '/comment', body)
      .pipe(catchError(this.handleError));
  }

  updateComment(vodId: string, commentId: string, body: any): Observable<any> {
    return this.http.put(this.serverUrl + '/' + vodId + '/comment/' + commentId, body)
      .pipe(catchError(this.handleError));
  }

  deleteComment(vodId: string, commentId: string): Observable<any> {
    return this.http.delete(this.serverUrl + '/' + vodId + '/comment/' + commentId)
      .pipe(catchError(this.handleError));
  }

  updateOnOff(vodId, isOnOff): Observable<any> {
    return this.http.put(this.serverUrl + '/' + vodId + '/switch', { isOnOff })
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  // in, out
  private attendance: string;

  // current Room data
  private currentRoom: any = null;

  constructor(
  ) { }

  public isAttended() {
    return this.attendance ? this.attendance === 'in' ? true : false : true;
  }

  public setAttendance(attendance) {
    this.attendance = attendance;
  }

  public getAttendance() {
    return this.attendance;
  }

  public setRoom(room) {
    this.currentRoom = room;
  }

  public getRoom() {
    return this.currentRoom;
  }

}

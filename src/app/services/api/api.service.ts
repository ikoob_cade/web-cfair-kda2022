import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  env = environment;

  constructor(private http: HttpClient) { }

  get(endpoint: string, body?: any): Observable<any> {
    return this.http.get(this.env.base_url + endpoint, body)
      .pipe(catchError(this.handleError));
  }

  post(endpoint: string, body?: any): Observable<any> {
    return this.http.post(this.env.base_url + endpoint, body )
      .pipe(catchError(this.handleError));
  }

  put(endpoint: string, body?: any): Observable<any> {
    return this.http.put(this.env.base_url + endpoint, body )
      .pipe(catchError(this.handleError));
  }

  delete(endpoint: string): Observable<any> {
    return this.http.delete(this.env.base_url + endpoint)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }


}

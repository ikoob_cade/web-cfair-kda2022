import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { EventService } from "../api/event.service";

@Injectable()
export class FunctionService {
    constructor(
        private router: Router,
        private eventService: EventService,
    ) { }

    scrollToTop(): void {
        window.scrollTo({ top: 0 });
    }

    /**
     * 배열 나누기
     * @param array 배열
     * @param n n개씩 자르기
     */
    division(array, n): any {
        return [].concat.apply([],
            array.map((item, i) => {
                return i % n ? [] : [array.slice(i, i + n)];
            })
        );
    }

    async checkEventVersion(eventParam?: any): Promise<void> {
        const event = eventParam ? eventParam : await this.eventService.checkEventVersion(); // 버전확인
        if (event.clientVersion) {
            const clientVersion = localStorage.getItem(
                event.eventCode + 'ver'
            );
            if (!clientVersion) {
                localStorage.setItem(
                    event.eventCode + 'ver',
                    event.clientVersion
                );
                location.reload();
            } else if (clientVersion) {
                if (clientVersion !== event.clientVersion) {
                    localStorage.setItem(
                        event.eventCode + 'ver',
                        event.clientVersion
                    );
                    location.reload();
                }
            }
        }
    }


}

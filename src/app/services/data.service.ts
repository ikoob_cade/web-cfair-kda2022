import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {
    private isReceiveMessage: BehaviorSubject<any> = new BehaviorSubject(null);
    currentReceiveMessage = this.isReceiveMessage.asObservable();

    private isForceLogout: BehaviorSubject<any> = new BehaviorSubject(null);
    forceLogout = this.isForceLogout.asObservable();

    private isMultiLogin: BehaviorSubject<any> = new BehaviorSubject(null);
    multiLogin = this.isMultiLogin.asObservable();

    constructor() {
    }

    changeReceiveMessage(data: any): void {
        this.isReceiveMessage.next(data);
    }

    doForceLogout(data:any): void {
        this.isForceLogout.next(data);
    }

    doMultiLogin(data:any): void {
        this.isMultiLogin.next(data);
    }
}

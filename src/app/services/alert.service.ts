import { ElementRef, Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../environments/environment';

// 팝업 서비스
@Injectable()
export class AlertService {
  constructor(private cookieService: CookieService) {}

  /** 메인 알림팝업 쿠키 확인 */
  mainPopupWithCookie(alertBtn: ElementRef): void {
    if (this.cookieService.get(`${environment.eventId}_main_popup`)) {
      const cookieTime = this.cookieService.get(
        `${environment.eventId}_main_popup`
      );
      const now = new Date().getTime().toString();
      if (cookieTime < now) {
        this.cookieService.delete(`${environment.eventId}_main_popup`);
        alertBtn.nativeElement.click();
      }
    } else {
      //   const first = this.dayjsService.makeTime('2021-11-05 20:00'); // 점검 팝업용
      //   const end = this.dayjsService.makeTime('2021-11-05 20:10');
      //   const now = this.dayjsService.makeTime();

      // if (now.isSameOrAfter(first) && now.isSameOrBefore(end)) {
      // ! 메인 팝업
      setTimeout(() => {
        alertBtn.nativeElement.click();
      }, 0);
      // }
    }
  }
}

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1', // 개발 API
  // base_url: 'http://192.168.1.11:8038/api/v1', // 고진혁 API
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 상용 API

  // eventId: '6242bcfe3ce8030500dfd8ef', // 개발 evId
  eventId: '624fe1801a3f9400121d9321', // 상용 evId

  socket_url: 'https://cfair-socket-ssl.cf-air.com', // 상용 소켓
  // socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr', // 개발 소켓
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

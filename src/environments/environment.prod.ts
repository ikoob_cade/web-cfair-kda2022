export const environment = {
  production: true,
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  socket_url: 'https://cfair-socket-ssl.cf-air.com',
  eventId: '624fe1801a3f9400121d9321' // 대한당뇨병학회 KDA 2022 Prod
};
